package jp.aegif.alfresco.sample;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.ResultSetRow;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.PermissionService;
import org.springframework.extensions.webscripts.Cache;
import org.springframework.extensions.webscripts.DeclarativeWebScript;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptRequest;

public class SampleSearchWebScript extends DeclarativeWebScript {
	
	private SearchService searchService;
	private NodeService nodeService;
	private PermissionService permissionService;
	
	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}
	
	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}
	
	public void setPermissionService(PermissionService permissionService) {
		this.permissionService = permissionService;
	}
	
    protected Map<String, Object> executeImpl(WebScriptRequest req, Status status, Cache cache) {
        Map<String, Object> model = new HashMap<String, Object>();

        String searchTerm = req.getParameter("q");
        if (searchTerm != null && !searchTerm.isEmpty()) {
            ResultSet resultSet  = searchService.query(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, SearchService.LANGUAGE_FTS_ALFRESCO, searchTerm);
                    
            List<Map<String, String>> results = new ArrayList<Map<String, String>>();
            for (ResultSetRow resultSetRow : resultSet) {
    			NodeRef nodeRef = resultSetRow.getNodeRef();
    			String name = (String)nodeService.getProperty(nodeRef, ContentModel.PROP_NAME);
    			String path = (String)nodeService.getPath(nodeRef).toDisplayPath(nodeService, permissionService);
    			Map<String, String> result = new HashMap<String, String>();
    			result.put("name", name);
    			result.put("path", path);
    			results.add(result);
    		}
           	model.put("results", results);
        }

        return model;
    }
}