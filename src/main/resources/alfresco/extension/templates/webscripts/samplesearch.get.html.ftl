<html>
  <head><title>Search Sample Web Script</title></head>
<body>
<#if results?has_content>
  <table>
    <tr><th></th><th>name</th><th>path</th></tr>
<#list results as node>
    <tr>
      <td><img src="${url.context}${node.icon16}"/></td>
      <td>${node.name}</td>
      <td>${node.displayPath}</td>
    </tr>
</#list>
  </table>
<#else>
<p>No search results</p>
</#if>
</body>
</html>
