<html>
  <head><title>Search Sample Java-backed Web Scripts</title></head>
<body>
<div><p>Java-backed Web Scripts Sample</p></div>
<#if results?has_content>
  <table>
    <tr><th>name</th><th>path</th></tr>
<#list results as node>
    <tr><td>${node.name}</td><td>${node.path}</td></tr>
</#list>
  </table>
<#else>
<p>No search results</p>
</#if>
</body>
</html>
